package metadata_fetchers

import (
	"gitlab.com/spacetimi/shared/timi_shared_server/code/config"
	"gitlab.com/spacetimi/shared/timi_shared_server/code/core/services/metadata_service/metadata_typedefs"
	"gitlab.com/spacetimi/shared/timi_shared_server/utils/logger"
)

func NewMetadataFetcher(metadataSpace metadata_typedefs.MetadataSpace) metadata_typedefs.IMetadataFetcher {
	var fetcher metadata_typedefs.IMetadataFetcher

	if metadataSpace == metadata_typedefs.METADATA_SPACE_SHARED {
		if config.GetEnvironmentConfiguration().SharedMetadataSourceURL == "local" {
			fetcher = NewMetadataFetcherFilesystem(config.GetSharedMetadataFilesPath())
		} else {
			fetcher = NewMetadataFetcherS3(config.GetEnvironmentConfiguration().SharedMetadataSourceURL, config.GetEnvironmentConfiguration().AdminToolConfig.SharedMetadataS3BucketName)
		}
	}

	if metadataSpace == metadata_typedefs.METADATA_SPACE_APP {
		if config.GetEnvironmentConfiguration().AppMetadataSourceURL == "local" {
			fetcher = NewMetadataFetcherFilesystem(config.GetAppMetadataFilesPath())
		} else {
			fetcher = NewMetadataFetcherS3(config.GetEnvironmentConfiguration().AppMetadataSourceURL, config.GetEnvironmentConfiguration().AdminToolConfig.AppMetadataS3BucketName)
		}
	}

	if fetcher == nil {
		logger.LogFatal("Unable to create metadata fetcher|metadataspace=" + string(metadataSpace))
		return nil
	}

	return fetcher
}
