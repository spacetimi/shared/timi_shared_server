package admin

import (
	"html/template"
	"net/http"

	"gitlab.com/spacetimi/shared/timi_shared_server/code/config"
	"gitlab.com/spacetimi/shared/timi_shared_server/utils/logger"
)

func showAdminPage(httpResponseWriter http.ResponseWriter, request *http.Request, adminPageObject AdminPageObject) {

	templates, err := template.ParseGlob(config.GetSharedTemplateFilesPath() + "/admin_tool/*")
	err = templates.ExecuteTemplate(httpResponseWriter, "admin_page_home_template.html", adminPageObject)

	if err != nil {
		logger.LogError("Error executing templates" +
			"|request url=" + request.URL.String() +
			"|error=" + err.Error())
		httpResponseWriter.WriteHeader(http.StatusInternalServerError)
	}
}
